User.create!(email: "example@railstutorial.org",
             password: "foobar",
             activated: true,
             activated_at: Time.zone.now,
						 city: "Raleigh",
						 state: "NC",
						 zip: 27612)