class QuestionPolicy < ApplicationPolicy
  def update?
    user.moderator? or not record.published?
  end
end