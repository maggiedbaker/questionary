class UserSerializer < ActiveModel::Serializer
  attributes :email, :city, :state, :zip
end