class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]
  before_action :check_logged_in, only: [:create, :edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      render :json => {
        "status": 201,
        "response": {
          "messages": ["Email sent with new password", "Please log in with new password, and then change it in your account settings."],
          "route": "string",
          "errors": {},
          "data": @user.reset_token
        }
      }
    else
      render :json => {
        "status": 500,
        "response": {
          "messages": ["Email address not found"],
          "route": "string",
          "errors": {},
          "data": {}
        }
      }
    end
  end

  def edit
  end

  def update
    if params[:password].empty?
      @user.errors.add(:password, "can't be empty")
      render :json => {
        "status": 500,
        "response": {
          "messages": [ "Can't be empty"],
          "route": "",
          "errors": @user.errors,
          "data": {}
        }
      }
    elsif @user.update_attributes(user_params)
      log_in @user
      render :json => {
        "status": 201,
        "response": {
          "messages": [ "Password has been reset"],
          "route": "",
          "errors": @user.errors,
          "data": {}
        }
      }
    end
  end

  private

    def user_params
      params.permit(:password)
    end

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # Confirms a valid user
    def valid_user
      unless (@user && @user.activated? && @user.authenticated?(:reset, params[:id]))
        render :json => {
          "status": 500,
          "response": {
            "messages": [ "Not valid user"],
            "route": "string",
            "errors": {},
            "data": {}
          }
        }
      end
    end

    # Checks expiration of reset token
    def check_expiration
      if @user.password_reset_expired?
        render :json => {
          "status": 500,
          "response": {
            "messages": [ "Password reset has expired"],
            "route": "new password reset url",
            "errors": {},
            "data": {}
          }
        }
      end
    end

    # Logs out if user tries to reset password while logged in
    def check_logged_in
      if logged_in?
        render :json => {
          "status": 500,
          "response": {
            "messages": ["You cannot begin the reset password process when you're already logged in. Please log out to try again."],
            "errors": {},
            "data": {}
          }
        }
      end
    end
  
end
