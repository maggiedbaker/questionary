class ApplicationController < ActionController::API
  include Pundit
  def protect_from_forgery
  end
	include SessionsHelper
  include ActionController::Cookies
end
