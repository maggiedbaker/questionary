class UsersController < ApplicationController
	before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :correct_user,   only: [:edit, :update, :destroy]
	# before_action :moderator_user, only: :destroy

		def index
			@users = User.all
			render :json => {
				"status": 201,
				"response": {
					"messages": [],
					"route": "string",
					"errors": {},
					"data": @users
				}
			}
		end
		
		def new
			@user = User.new
		end

		def create
			@user = User.new(user_params)
			if @user.save
				@user.send_activation_email
				render :json => {
					"status": 201,
					"response": {
						"messages": ["Please check your email to activate your account."],
						"route": "string",
						"errors": {},
						"data": @user.activation_token
					}
				}
			else
				render :json => {
					"status": 500,
					"response": {
						"messages": [],
						"route": "string",
						"errors": @user.errors,
						"data": {}
					}
				}
			end
		end

		def show
			@user = User.find_by_id(params[:id])
			if @user
				render :json => {
					"status": 201,
					"response": {
						"messages": [],
						"route": "string",
						"errors": @user.errors,
						"data": current_user
					}
				}
			else
				render :json => {
					"status": 500,
					"response": {
						"messages": [],
						"route": "string",
						"errors": @user.errors,
						"data": {}
					}
				}
			end
		end

		def update
			@user = User.find_by_id(params[:id])
			if @user
				if @user.update(user_params)
					render :json => {
						"status": 200,
						"response": {
							"messages": [ "Profile updated" ],
							"route": "string",
							"errors": @user.errors,
							"data": @user
						}
					}
				else
					render :json => {
						"status": 500,
						"response": {
							"messages": [],
							"route": "string",
							"errors": @user.errors,
							"data": {}
						}
					}
				end
			else
				render :json => {
						"status": 500,
						"response": {
							"messages": [],
							"route": "string",
							"errors": @user.errors,
							"data": @user
						}
					}
			end
		end

		def destroy
			User.find(params[:id]).destroy
			render :json => {
				"status": 200,
				"response": {
					"messages": [ "User deleted" ],
					"route": "string",
					"errors": {},
					"data": @user
				}
			}
		end

		private
		
    def user_params
      params.permit(:email, :password, :password_confirmation, :city, :state, :zip)
    end

		# Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
      	render :json => {
					"status": 401,
					"response": {
						"messages": [ "Please log in." ],
						"route": "/login",
						"errors": {},
						"data": {}
					}
				}
      end
    end
		
		def correct_user
			@user = User.find(params[:id])
			unless current_user?(@user) || current_user.moderator?
				render :json => {
					"status": 401,
					"response": {
						"messages": [ "You do not have permission to do that." ],
						"route": "/",
						"errors": {},
						"data": {}
					}
				}
			end
		end

		def moderator_user
			unless current_user.moderator?
				render :json => {
					"status": 401,
					"response": {
						"messages": [ "You do not have permission to do that." ],
						"route": "/",
						"errors": {},
						"data": {}
					}
				}
			end
		end

end
