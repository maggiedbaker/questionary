class QuestionsController < ApplicationController
	before_action :logged_in_user
  before_action :correct_user, only: [:edit, :update, :destroy]

	def index
		@questions = Question.all
		render :json => {
			"status": 201,
			"response": {
				"messages": [],
				"route": "string",
				"errors": {},
				"data": @questions
			}
		}
	end

	def new
		@question = Question.new
	end

	def create
		@question = current_user.questions.new(question_params)
		if @question.save
			render :json => {
				"status": 201,
				"response": {
					"messages": ["Question created."],
					"route": "string",
					"errors": {},
					"data": @question
				}
			}
		else
			render :json => {
				"status": 500,
				"response": {
					"messages": ["Could not create question"],
					"route": "string",
					"errors": @user.errors,
					"data": {}
				}
			}
		end
	end

	def show
		@question = Question.find_by_id(params[:id])
		if @question
			render :json => {
				"status": 201,
				"response": {
					"messages": [],
					"route": "string",
					"errors": @question.errors,
					"data": @question
				}
			}
		else
			render :json => {
				"status": 500,
				"response": {
					"messages": [],
					"route": "string",
					"errors": @question.errors,
					"data": {}
				}
			}
		end
	end

	def edit
	end

	def update
		@question = Question.find_by_id(params[:id])
		if @question
			if @question.update(question_params)
				render :json => {
					"status": 200,
					"response": {
						"messages": [ "Question updated" ],
						"route": "string",
						"errors": @question.errors,
						"data": @question
					}
				}
			else
				render :json => {
					"status": 500,
					"response": {
						"messages": [],
						"route": "string",
						"errors": @question.errors,
						"data": {}
					}
				}
			end
		else
			render :json => {
					"status": 500,
					"response": {
						"messages": [],
						"route": "string",
						"errors": @question.errors,
						"data": @question
					}
				}
		end
	end

	def destroy
		Question.find(params[:id]).destroy
		render :json => {
			"status": 200,
			"response": {
				"messages": [ "Question deleted" ],
				"route": "string",
				"errors": {},
				"data": {}
			}
		}
	end

	private

	def question_params
		params.permit(:text)
	end

  def logged_in_user
    unless logged_in?
		  render :json => {
				"status": 401,
				"response": {
					"messages": [ "Please log in." ],
					"route": "/login",
					"errors": {},
					"data": {}
				}
			}
		end
	end
		
	def correct_user
			@user = Question.find(params[:id]).user
			unless current_user?(@user) || current_user.moderator?
				render :json => {
					"status": 401,
					"response": {
						"messages": [ "You do not have permission to do that." ],
						"route": "/",
						"errors": {},
						"data": {}
					}
				}
			end
	end


end
