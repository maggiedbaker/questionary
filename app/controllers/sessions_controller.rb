class SessionsController < ApplicationController
  
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        render :json => {
          "status": 201,
          "response": {
            "messages": [ "You are now logged in!"],
            "route": "string",
            "errors": user.errors,
            "data": user
          }
        }
      else
        render :json => {
          "status": 500,
          "response": {
            "messages": ["Invalid email/password combination"],
            "route": "string",
            "errors": {},
            "data": {}
          }
        }
      end
    else
      render :json => {
        "status": 500,
        "response": {
          "messages": ["Invalid email/password combination"],
          "route": "string",
          "errors": {},
          "data": {}
        }
			}
    end
  end

  def destroy
    log_out if logged_in?
    render :json => {
      "status": 500,
      "response": {
        "messages": ["You are logged out."],
        "route": "string",
        "errors": {},
        "data": {}
      }
		}
  end

end
