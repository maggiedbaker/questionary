class AccountActivationsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:code])
      user.update_attribute(:activated, true)
      user.update_attribute(:activated_at, Time.zone.now)
      user.activate
      log_in user
      render :json => {
        "status": 201,
        "response": {
          "messages": ["Account Activated!"],
          "route": "string",
          "errors": {},
          "data": current_user
        }
      }
    else
      render :json => {
        "status": 500,
        "response": {
          "messages": ["Invalid activation code"],
          "route": "string",
          "errors": {},
          "data": {}
        }
      }
    end
  end

end
