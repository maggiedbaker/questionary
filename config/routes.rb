Rails.application.routes.draw do

  get 'password_resets/new'

  get 'password_resets/edit'

  root "welcome#hello"
  

  get "/signup" => "users#new"
  get "/login" => "sessions#new"
  post "/login" => "sessions#create"
  delete "/logout" => "sessions#destroy"

  resources :users, :path => "user"
  resources :questions
  resources :account_activations, only: [:new, :create]
  resources :password_resets, only: [:new, :create, :edit, :update]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
